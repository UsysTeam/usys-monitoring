﻿using Usys.Monitor.Agent.Services;

namespace Usys.Monitor.Agent
{
    public class AgentServiceStarter
    {
        private SystemHarvestingService _systemHarvestingService;

        public void Start()
        {
            _systemHarvestingService = new SystemHarvestingService();
            _systemHarvestingService.Start();
        }
    }
}
