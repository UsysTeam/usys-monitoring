﻿using System.Configuration;

namespace Usys.Monitor.Agent.Configuration
{
    public class ServiceThreadConfigurationElement : ConfigurationElement
    {
        [ConfigurationProperty("sleep", DefaultValue = "10", IsRequired = true)]
        public int SleepTime
        {
            get { return (int)this["sleep"]; }
            set { this["sleep"] = value; }
        }
    }
}