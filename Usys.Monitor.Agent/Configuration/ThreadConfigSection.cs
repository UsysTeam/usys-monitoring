﻿using System.Configuration;

namespace Usys.Monitor.Agent.Configuration
{
    internal class ThreadConfigSection : ConfigurationSection
    {
        [ConfigurationProperty("updateServiceConfig", IsRequired = false)]
        public UpdateServiceConfigServiceThreadElement UpdateServiceConfig
        {
            get { return (UpdateServiceConfigServiceThreadElement)this["updateServiceConfig"]; }
            set { this["updateServiceConfig"] = value; }
        }

        [ConfigurationProperty("registerEntries", IsRequired = false)]
        public RegisterEntriesServiceThreadElement RegisterEntries
        {
            get { return (RegisterEntriesServiceThreadElement)this["registerEntries"]; }
            set { this["registerEntries"] = value; }
        }

        [ConfigurationProperty("generalPerformance", IsRequired = false)]
        public GeneralPerformanceServiceThreadElement GeneralPerformance
        {
            get { return (GeneralPerformanceServiceThreadElement)this["generalPerformance"]; }
            set { this["generalPerformance"] = value; }
        }

        [ConfigurationProperty("updateConfigurationSection", IsRequired = false)]
        public UpdateConfigurationSectionServiceThreadElement UpdateConfigurationSection
        {
            get { return (UpdateConfigurationSectionServiceThreadElement)this["updateConfigurationSection"]; }
            set { this["updateConfigurationSection"] = value; }
        }

        [ConfigurationProperty("mvcPerformance", IsRequired = false)]
        public MvcPerformanceServiceThreadElement MvcPerformance
        {
            get { return (MvcPerformanceServiceThreadElement)this["mvcPerformance"]; }
            set { this["mvcPerformance"] = value; }
        }

        [ConfigurationProperty("wcfPerformance", IsRequired = false)]
        public WcfPerformanceServiceThreadElement WcfPerformance
        {
            get { return (WcfPerformanceServiceThreadElement)this["wcfPerformance"]; }
            set { this["wcfPerformance"] = value; }
        }

        [ConfigurationProperty("telnetPerformance", IsRequired = false)]
        public TelnetPerformanceServiceThreadElement TelnetPerformance
        {
            get { return (TelnetPerformanceServiceThreadElement)this["telnetPerformance"]; }
            set { this["telnetPerformance"] = value; }
        }

        [ConfigurationProperty("docGenPerformance", IsRequired = false)]
        public DocGenPerformanceServiceThreadElement DocGenPerformance
        {
            get { return (DocGenPerformanceServiceThreadElement)this["docGenPerformance"]; }
            set { this["docGenPerformance"] = value; }
        }
    }
}
