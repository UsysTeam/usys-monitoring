﻿using System;
using System.Linq;
using System.Management;

namespace Usys.Monitor.Agent.Helpers
{
    public class WmiPerformanceCounter : IDisposable
    {
        private ManagementObjectSearcher _query;

        public WmiPerformanceCounter(string query)
        {
            _query = new ManagementObjectSearcher(query);
        }

        public WmiPerformanceCounter()
        {
            
        }

        public TOutput NextValue<TOutput>(string field)
        {
            var result = _query.Get().Cast<ManagementObject>().FirstOrDefault()[field].ToString();

            return (TOutput)Convert.ChangeType(result, typeof (TOutput));
        }

        public TOutput NextValue<TOutput>(string query, string field)
        {
            var result = new ManagementObjectSearcher(query).Get().Cast<ManagementObject>().FirstOrDefault()[field].ToString();

            return (TOutput)Convert.ChangeType(result, typeof (TOutput));
        }

        public void Dispose()
        {
            if (_query != null)
            {
                _query.Dispose();
                _query = null;
            }
        }
    }
}
