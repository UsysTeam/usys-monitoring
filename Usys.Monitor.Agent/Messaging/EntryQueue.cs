﻿using System;
using System.Collections.Generic;
using System.Linq;
using Usys.Monitor.Data.Model;

namespace Usys.Monitor.Agent.Messaging
{
    public static class EntryQueue
    {
        private static readonly object QueueLock = new object();
        private static readonly List<EntryBase> Queue = new List<EntryBase>();
        private static readonly string MachineName = Environment.MachineName;

        public static void Register(EntryBase entry)
        {
            lock (QueueLock)
            {
                Queue.Add(entry);
            }

            LimitEntriesOnQueue();
        }

        public static void Register(IEnumerable<EntryBase> entries)
        {
            lock (QueueLock)
            {
                Queue.AddRange(entries);
            }

            LimitEntriesOnQueue();
        }

        public static List<EntryBase> GetEntries(int? max = null)
        {
            var dynamicMax = max.GetValueOrDefault(Math.Max(100, Queue.Count / 5));

            var entries = Queue.OrderByDescending(e => e.CreationDateTime).Take(dynamicMax).ToList();

            lock (QueueLock)
            {
                Queue.RemoveAll(entries.Contains);
            }

            return entries;
        }

        public static TEntry Prepare<TEntry>()
            where TEntry : EntryBase, new()
        {
            var entry = new TEntry { Id = Guid.NewGuid(), MachineName = MachineName, CreationDateTime = DateTime.UtcNow };

            return entry;
        }

        private static void LimitEntriesOnQueue()
        {
            if (Queue.Count <= 1000)
                return;

            lock (QueueLock)
            {
                var entries = Queue.OrderByDescending(e => e.CreationDateTime).Skip(1000).Take(10).ToList();
                Queue.RemoveAll(entries.Contains);
            }
        }
    }
}
