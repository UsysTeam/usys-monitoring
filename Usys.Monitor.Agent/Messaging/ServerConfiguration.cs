﻿namespace Usys.Monitor.Agent.Messaging
{
    public static class ServerConfiguration
    {
        public static bool IsTelnetServer { get; set; }
        public static bool IsDocGenServer { get; set; }
        public static bool IsMvcServer { get; set; }
        public static bool IsWcfServer { get; set; }
    }
}
