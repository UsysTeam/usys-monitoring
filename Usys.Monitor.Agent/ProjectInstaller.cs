﻿using System.ComponentModel;
using System.Configuration.Install;
using System.ServiceProcess;

namespace Usys.Monitor.Agent
{
    [RunInstaller(true)]
    public class ProjectInstaller : Installer
    {
        private ServiceProcessInstaller process;
        private ServiceInstaller harvesterService;

        public ProjectInstaller()
        {
            process = new ServiceProcessInstaller();
            process.Account = ServiceAccount.User;
            process.Username = "VanEyndS@prdlog.local";
            process.Password = "Univeg01";

            harvesterService = new ServiceInstaller();
            harvesterService.ServiceName = "Usys Monitor Agent";
            harvesterService.StartType = ServiceStartMode.Automatic;

            Installers.Add(process);
            Installers.Add(harvesterService);
        }
    }
}
