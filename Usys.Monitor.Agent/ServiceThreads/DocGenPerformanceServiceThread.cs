using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Usys.Monitor.Agent.Helpers;
using Usys.Monitor.Agent.Messaging;
using Usys.Monitor.Core.Diagnostics;
using Usys.Monitor.Data.Model;

namespace Usys.Monitor.Agent.ServiceThreads
{
    public class DocGenPerformanceServiceThread : ServiceThreadBase
    {
        private readonly Dictionary<string, WmiPerformanceCounter> _docGenProcessorUsageCounters = new Dictionary<string, WmiPerformanceCounter>();
        private readonly Dictionary<string, PerformanceCounter> _docGenProcessIdCounters = new Dictionary<string, PerformanceCounter>();
        private readonly Dictionary<string, PerformanceCounter> _docGenMemoryUsageCounters = new Dictionary<string, PerformanceCounter>();
        private readonly Dictionary<string, PerformanceCounter> _docGenThreadCountreCounters = new Dictionary<string, PerformanceCounter>();

        protected override ThreadStart ConfigureThreadAction()
        {
            while (true)
            {
                if (ServerConfiguration.IsDocGenServer)
                {
                    HarvestDocGenProcessResourceUsage();
                }

                Thread.Sleep(TimeSpan.FromSeconds(ThreadConfigSection.DocGenPerformance.SleepTime));
            }
        }

        private void HarvestDocGenProcessResourceUsage()
        {
            var cat = new PerformanceCounterCategory("Process");
            var instances = cat.GetInstanceNames().Where(instance => instance.Contains("DocumentGeneration"));

            Parallel.ForEach(instances, instance =>
            {
                try
                {
                    var entry = EntryQueue.Prepare<DocGenProcessInfo>();
                    entry.InstanceName = instance.Replace("#", "_");

                    if (!_docGenProcessIdCounters.ContainsKey(instance))
                        _docGenProcessIdCounters[instance] = new PerformanceCounter("Process", "ID Process", instance, true);
                    entry.ProcessId = (int)_docGenProcessIdCounters[instance].NextValue();

                    if (!_docGenMemoryUsageCounters.ContainsKey(instance))
                        _docGenMemoryUsageCounters[instance] = new PerformanceCounter("Process", "Working Set - Private", instance, true);
                    entry.MemoryUsage = _docGenMemoryUsageCounters[instance].NextValue();

                    if (!_docGenThreadCountreCounters.ContainsKey(instance))
                        _docGenThreadCountreCounters[instance] = new PerformanceCounter("Process", "Thread Count", instance, true);
                    entry.ThreadCount = (int)_docGenThreadCountreCounters[instance].NextValue();

                    if (!_docGenProcessorUsageCounters.ContainsKey(instance))
                        _docGenProcessorUsageCounters[instance] = new WmiPerformanceCounter();//new PerformanceCounter("Process", "% Processor Time", instance, true);
                    entry.ProcessorUsage = _docGenProcessorUsageCounters[instance].NextValue<float>("SELECT PercentProcessorTime FROM Win32_PerfFormattedData_PerfProc_Process WHERE IDProcess =" + entry.ProcessId, "PercentProcessorTime");

                    foreach (var item in new System.Management.ManagementObjectSearcher("Select NumberOfProcessors, NumberOfLogicalProcessors from Win32_ComputerSystem").Get())
                    {
                        entry.NumberOfProcessors += int.Parse(item["NumberOfProcessors"].ToString());
                        entry.NumberOfLogicalProcessors += int.Parse(item["NumberOfLogicalProcessors"].ToString());
                    }

                    foreach (var item in new System.Management.ManagementObjectSearcher("Select NumberOfCores from Win32_Processor").Get())
                    {
                        entry.NumberOfCores += int.Parse(item["NumberOfCores"].ToString());
                    }

                    EntryQueue.Register(entry);
                }
                catch (Exception exception)
                {
                    LogManager.WriteError("Unable to harvest docgen process resource usage: " + exception.Message);
                }
            });
        }
    }
}