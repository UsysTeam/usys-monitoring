using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Management;
using System.Threading;
using Usys.Monitor.Agent.Helpers;
using Usys.Monitor.Agent.Messaging;
using Usys.Monitor.Core.Diagnostics;
using Usys.Monitor.Data.Model;

namespace Usys.Monitor.Agent.ServiceThreads
{
    public class GeneralPerformanceServiceThread : ServiceThreadBase
    {
        private WmiPerformanceCounter _totalProcessorUsage;
        private PerformanceCounter _totalCommittedBytes;

        protected override ThreadStart ConfigureThreadAction()
        {
            while (true)
            {
                HarvestGeneralDriveInfo();
                HarvestGeneralResourceUsage();

                Thread.Sleep(TimeSpan.FromSeconds(ThreadConfigSection.GeneralPerformance.SleepTime));
            }
        }

        public override void Start()
        {
            _totalProcessorUsage = new WmiPerformanceCounter("SELECT PercentProcessorTime FROM Win32_PerfFormattedData_PerfOS_Processor WHERE Name LIKE '_Total'");// new PerformanceCounter("Processor", "% Processor Time", "_Total");
            _totalCommittedBytes = new PerformanceCounter("Memory", "% Committed Bytes In Use");

            base.Start();
        }

        public override void Stop()
        {
            try
            {
                if (_totalProcessorUsage != null)
                {
                    _totalProcessorUsage.Dispose();
                    _totalProcessorUsage = null;
                }

                if (_totalCommittedBytes != null)
                {
                    _totalCommittedBytes.Dispose();
                    _totalCommittedBytes = null;
                }
            }
            catch { }

            base.Stop();
        }

        private void HarvestGeneralDriveInfo()
        {
            try
            {
                var drives = DriveInfo.GetDrives();
                var mainDrive = drives.FirstOrDefault(drive => drive.Name.ToLowerInvariant() == "c:\\");

                if (mainDrive != null)
                {
                    var entry = EntryQueue.Prepare<GeneralDriveInfo>();
                    entry.AvailableFreeSpace = mainDrive.AvailableFreeSpace;
                    entry.TotalFreeSpace = mainDrive.TotalFreeSpace;
                    entry.TotalSize = mainDrive.TotalSize;
                    EntryQueue.Register(entry);
                }
            }
            catch (Exception exception)
            {
                LogManager.WriteError("Unable to harvest general drive info: " + exception.Message);
            }
        }

        private void HarvestGeneralResourceUsage()
        {
            try
            {
                var entry = EntryQueue.Prepare<GeneralSystemInfo>();
                entry.MemoryUsage = _totalCommittedBytes.NextValue();
                entry.ProcessorUsage = _totalProcessorUsage.NextValue<float>("PercentProcessorTime");

                foreach (var item in new ManagementObjectSearcher("Select NumberOfProcessors, NumberOfLogicalProcessors from Win32_ComputerSystem").Get())
                {
                    entry.NumberOfProcessors += int.Parse(item["NumberOfProcessors"].ToString());
                    entry.NumberOfLogicalProcessors += int.Parse(item["NumberOfLogicalProcessors"].ToString());
                }

                foreach (var item in new ManagementObjectSearcher("Select NumberOfCores from Win32_Processor").Get())
                {
                    entry.NumberOfCores += int.Parse(item["NumberOfCores"].ToString());
                }

                EntryQueue.Register(entry);
            }
            catch (Exception exception)
            {
                LogManager.WriteError("Unable to harvest general resource usage: " + exception.Message);
            }
        }
    }
}