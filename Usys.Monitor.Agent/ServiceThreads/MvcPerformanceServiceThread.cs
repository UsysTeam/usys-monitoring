using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Usys.Monitor.Agent.Helpers;
using Usys.Monitor.Agent.Messaging;
using Usys.Monitor.Core.Diagnostics;
using Usys.Monitor.Data.Model;

namespace Usys.Monitor.Agent.ServiceThreads
{
    public class MvcPerformanceServiceThread : ServiceThreadBase
    {
        private readonly Dictionary<string, WmiPerformanceCounter> _mvcProcessorUsageCounters = new Dictionary<string, WmiPerformanceCounter>();
        private readonly Dictionary<string, PerformanceCounter> _mvcProcessIdCounters = new Dictionary<string, PerformanceCounter>();
        private readonly Dictionary<string, PerformanceCounter> _mvcMemoryUsageCounters = new Dictionary<string, PerformanceCounter>();
        private readonly Dictionary<string, PerformanceCounter> _mvcThreadCountreCounters = new Dictionary<string, PerformanceCounter>();

        protected override ThreadStart ConfigureThreadAction()
        {
            while (true)
            {
                if (ServerConfiguration.IsMvcServer)
                {
                    HarvestMvcProcessResourceUsage();
                }

                Thread.Sleep(TimeSpan.FromSeconds(ThreadConfigSection.MvcPerformance.SleepTime));
            }
        }

        private void HarvestMvcProcessResourceUsage()
        {
            var cat = new PerformanceCounterCategory("Process");
            var instances = cat.GetInstanceNames().Where(instance => instance.Contains("w3wp"));

            Parallel.ForEach(instances, instance =>
            {
                try
                {
                    var entry = EntryQueue.Prepare<MvcProcessInfo>();
                    entry.InstanceName = instance.Replace("#", "_");

                    if (!_mvcProcessIdCounters.ContainsKey(instance))
                        _mvcProcessIdCounters[instance] = new PerformanceCounter("Process", "ID Process", instance, true);
                    entry.ProcessId = (int)_mvcProcessIdCounters[instance].NextValue();

                    if (!_mvcMemoryUsageCounters.ContainsKey(instance))
                        _mvcMemoryUsageCounters[instance] = new PerformanceCounter("Process", "Working Set - Private", instance, true);
                    entry.MemoryUsage = _mvcMemoryUsageCounters[instance].NextValue();

                    if (!_mvcThreadCountreCounters.ContainsKey(instance))
                        _mvcThreadCountreCounters[instance] = new PerformanceCounter("Process", "Thread Count", instance, true);
                    entry.ThreadCount = (int)_mvcThreadCountreCounters[instance].NextValue();

                    if (!_mvcProcessorUsageCounters.ContainsKey(instance))
                        _mvcProcessorUsageCounters[instance] = new WmiPerformanceCounter();
                    entry.ProcessorUsage = _mvcProcessorUsageCounters[instance].NextValue<float>("SELECT PercentProcessorTime FROM Win32_PerfFormattedData_PerfProc_Process WHERE IDProcess =" + entry.ProcessId, "PercentProcessorTime");

                    foreach (var item in new System.Management.ManagementObjectSearcher("Select NumberOfProcessors, NumberOfLogicalProcessors from Win32_ComputerSystem").Get())
                    {
                        entry.NumberOfProcessors += int.Parse(item["NumberOfProcessors"].ToString());
                        entry.NumberOfLogicalProcessors += int.Parse(item["NumberOfLogicalProcessors"].ToString());
                    }

                    foreach (var item in new System.Management.ManagementObjectSearcher("Select NumberOfCores from Win32_Processor").Get())
                    {
                        entry.NumberOfCores += int.Parse(item["NumberOfCores"].ToString());
                    }

                    EntryQueue.Register(entry);
                }
                catch (Exception exception)
                {
                    LogManager.WriteError("Unable to harvest mvc process resource usage: " + exception.Message);
                }
            });
        }
    }
}