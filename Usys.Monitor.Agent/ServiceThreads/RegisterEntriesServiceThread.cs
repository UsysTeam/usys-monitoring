using System;
using System.ServiceModel;
using System.Threading;
using Usys.Monitor.Agent.Messaging;
using Usys.Monitor.Core.Diagnostics;
using Usys.Monitor.Service.Contracts.ServiceContracts;

namespace Usys.Monitor.Agent.ServiceThreads
{
    public class RegisterEntriesServiceThread : ServiceThreadBase
    {
        private ChannelFactory<IAgentControllerService> _controllerChannelFactory;

        public RegisterEntriesServiceThread()
        {
            CreateChannelFactories();
        }

        private void CreateChannelFactories()
        {
            _controllerChannelFactory = new ChannelFactory<IAgentControllerService>("AgentControllerService");
        }

        protected override ThreadStart ConfigureThreadAction()
        {
            while (true)
            {
                var entries = EntryQueue.GetEntries();

                try
                {
                    IAgentControllerService channel;
                    using (((IClientChannel)(channel = _controllerChannelFactory.CreateChannel())))
                    {
                        if (!channel.RegisterEntries(entries))
                        {
                            EntryQueue.Register(entries);
                        }
                    }
                }
                catch (Exception exception)
                {
                    LogManager.WriteError("Unable to register entries: " + exception.Message + " (" + _controllerChannelFactory.Endpoint.Address.Uri + ")");
                    EntryQueue.Register(entries);
                }

                Thread.Sleep(TimeSpan.FromSeconds(ThreadConfigSection.RegisterEntries.SleepTime));
            }
        }
    }
}