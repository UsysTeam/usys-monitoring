﻿using System.Configuration;
using System.Threading;
using Usys.Monitor.Agent.Configuration;

namespace Usys.Monitor.Agent.ServiceThreads
{
    public abstract class ServiceThreadBase
    {
        private readonly Thread _thread;

        protected ServiceThreadBase()
        {
            _thread = new Thread(() => ConfigureThreadAction());
        }

        protected abstract ThreadStart ConfigureThreadAction();

        public virtual void Start()
        {
            if (_thread == null)
                return;

            _thread.Start();
        }

        public virtual void Stop()
        {
            if (_thread == null)
                return;

            StopRequested = true;

            _thread.Abort();
        }

        protected bool StopRequested { get; private set; }

        internal ThreadConfigSection ThreadConfigSection
        {
            get
            {
                var settings = ConfigurationManager.GetSection("threadConfigSettings");

                var section = settings as ThreadConfigSection;

                return section;
            }
        }
    }
}
