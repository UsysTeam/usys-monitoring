using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Usys.Monitor.Agent.Helpers;
using Usys.Monitor.Agent.Messaging;
using Usys.Monitor.Core.Diagnostics;
using Usys.Monitor.Data.Model;

namespace Usys.Monitor.Agent.ServiceThreads
{
    public class TelnetPerformanceServiceThread : ServiceThreadBase
    {
        private readonly Dictionary<string, WmiPerformanceCounter> _telnetProcessorUsageCounters = new Dictionary<string, WmiPerformanceCounter>();
        private readonly Dictionary<string, PerformanceCounter> _telnetProcessIdCounters = new Dictionary<string, PerformanceCounter>();
        private readonly Dictionary<string, PerformanceCounter> _telnetMemoryUsageCounters = new Dictionary<string, PerformanceCounter>();
        private readonly Dictionary<string, PerformanceCounter> _telnetThreadCountreCounters = new Dictionary<string, PerformanceCounter>();

        protected override ThreadStart ConfigureThreadAction()
        {
            while (true)
            {
                if (ServerConfiguration.IsTelnetServer)
                {
                    HarvestTelnetProcessResourceUsage();
                }

                Thread.Sleep(TimeSpan.FromSeconds(ThreadConfigSection.TelnetPerformance.SleepTime));
            }
        }

        private void HarvestTelnetProcessResourceUsage()
        {
            var cat = new PerformanceCounterCategory("Process");
            var instances = cat.GetInstanceNames().Where(instance =>
                instance.Contains("ScannerConsole")
                || instance.Contains("telnet"));

            Parallel.ForEach(instances, instance =>
            {
                try
                {
                    var entry = EntryQueue.Prepare<TelnetProcessInfo>();
                    entry.InstanceName = instance.Replace("#", "_");

                    if (!_telnetProcessIdCounters.ContainsKey(instance))
                        _telnetProcessIdCounters[instance] = new PerformanceCounter("Process", "ID Process", instance, true);
                    entry.ProcessId = (int)_telnetProcessIdCounters[instance].NextValue();

                    if (!_telnetMemoryUsageCounters.ContainsKey(instance))
                        _telnetMemoryUsageCounters[instance] = new PerformanceCounter("Process", "Working Set - Private", instance, true);
                    entry.MemoryUsage = _telnetMemoryUsageCounters[instance].NextValue();

                    if (!_telnetThreadCountreCounters.ContainsKey(instance))
                        _telnetThreadCountreCounters[instance] = new PerformanceCounter("Process", "Thread Count", instance, true);
                    entry.ThreadCount = (int)_telnetThreadCountreCounters[instance].NextValue();

                    if (!_telnetProcessorUsageCounters.ContainsKey(instance))
                        _telnetProcessorUsageCounters[instance] = new WmiPerformanceCounter();//"Process", "% Processor Time", instance, true);
                    entry.ProcessorUsage = _telnetProcessorUsageCounters[instance].NextValue<float>("SELECT PercentProcessorTime FROM Win32_PerfFormattedData_PerfProc_Process WHERE IDProcess =" + entry.ProcessId, "PercentProcessorTime");

                    foreach (var item in new System.Management.ManagementObjectSearcher("Select NumberOfProcessors, NumberOfLogicalProcessors from Win32_ComputerSystem").Get())
                    {
                        entry.NumberOfProcessors += int.Parse(item["NumberOfProcessors"].ToString());
                        entry.NumberOfLogicalProcessors += int.Parse(item["NumberOfLogicalProcessors"].ToString());
                    }

                    foreach (var item in new System.Management.ManagementObjectSearcher("Select NumberOfCores from Win32_Processor").Get())
                    {
                        entry.NumberOfCores += int.Parse(item["NumberOfCores"].ToString());
                    }

                    EntryQueue.Register(entry);
                }
                catch (Exception exception)
                {
                    LogManager.WriteError("Unable to harvest telnet process resource usage: " + exception.Message);
                }
            });
        }
    }
}