using System;
using System.Configuration;
using System.Threading;

namespace Usys.Monitor.Agent.ServiceThreads
{
    public class UpdateConfigurationSectionServiceThread : ServiceThreadBase
    {
        protected override ThreadStart ConfigureThreadAction()
        {
            while (true)
            {
                ConfigurationManager.RefreshSection("threadConfigSettings");

                Thread.Sleep(TimeSpan.FromSeconds(ThreadConfigSection.UpdateConfigurationSection.SleepTime));
            }
        }
    }
}