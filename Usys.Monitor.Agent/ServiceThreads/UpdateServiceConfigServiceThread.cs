﻿using System;
using System.ServiceModel;
using System.Threading;
using Usys.Monitor.Agent.Messaging;
using Usys.Monitor.Core.Diagnostics;
using Usys.Monitor.Service.Contracts.ServiceContracts;

namespace Usys.Monitor.Agent.ServiceThreads
{
    public class UpdateServiceConfigServiceThread : ServiceThreadBase
    {
        private ChannelFactory<IAgentControllerService> _controllerChannelFactory;

        public UpdateServiceConfigServiceThread()
        {
            CreateChannelFactories();
        }

        private void CreateChannelFactories()
        {
            _controllerChannelFactory = new ChannelFactory<IAgentControllerService>("AgentControllerService");
        }

        protected override ThreadStart ConfigureThreadAction()
        {
            Thread.Sleep(TimeSpan.FromSeconds(10));
            while (true)
            {
                try
                {
                    IAgentControllerService channel;
                    using (((IClientChannel)(channel = _controllerChannelFactory.CreateChannel())))
                    {
                        var serverConfig = channel.RetrieveServerConfig(Environment.MachineName);

                        if (serverConfig != null)
                        {
                            ServerConfiguration.IsDocGenServer = serverConfig.IsDocGenServer;
                            ServerConfiguration.IsMvcServer = serverConfig.IsMvcServer;
                            ServerConfiguration.IsTelnetServer = serverConfig.IsTelnetServer;
                            ServerConfiguration.IsWcfServer = serverConfig.IsWcfServer;
                        }
                    }
                }
                catch (Exception exception)
                {
                    LogManager.WriteError("Unable to retrieve server configs: " + exception.Message);
                }

                Thread.Sleep(TimeSpan.FromSeconds(ThreadConfigSection.UpdateServiceConfig.SleepTime));
            }
        }
    }
}
