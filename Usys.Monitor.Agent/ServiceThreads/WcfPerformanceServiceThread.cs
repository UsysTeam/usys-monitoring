using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Usys.Monitor.Agent.Helpers;
using Usys.Monitor.Agent.Messaging;
using Usys.Monitor.Core.Diagnostics;
using Usys.Monitor.Data.Model;

namespace Usys.Monitor.Agent.ServiceThreads
{
    public class WcfPerformanceServiceThread : ServiceThreadBase
    {
        private readonly Dictionary<string, WmiPerformanceCounter> _wcfProcessorUsageCounters = new Dictionary<string, WmiPerformanceCounter>();
        private readonly Dictionary<string, PerformanceCounter> _wcfProcessIdCounters = new Dictionary<string, PerformanceCounter>();
        private readonly Dictionary<string, PerformanceCounter> _wcfMemoryUsageCounters = new Dictionary<string, PerformanceCounter>();
        private readonly Dictionary<string, PerformanceCounter> _wcfThreadCountreCounters = new Dictionary<string, PerformanceCounter>();

        protected override ThreadStart ConfigureThreadAction()
        {
            while (true)
            {
                if (ServerConfiguration.IsWcfServer)
                {
                    HarvestWcfProcessResourceUsage();
                }

                Thread.Sleep(TimeSpan.FromSeconds(ThreadConfigSection.WcfPerformance.SleepTime));
            }
        }

        private void HarvestWcfProcessResourceUsage()
        {
            var cat = new PerformanceCounterCategory("Process");
            var instances = cat.GetInstanceNames().Where(instance => instance.Contains("w3wp"));

            Parallel.ForEach(instances, instance =>
            {
                try
                {
                    var entry = EntryQueue.Prepare<WcfProcessInfo>();
                    entry.InstanceName = instance.Replace("#", "_");

                    if (!_wcfProcessIdCounters.ContainsKey(instance))
                        _wcfProcessIdCounters[instance] = new PerformanceCounter("Process", "ID Process", instance, true);
                    entry.ProcessId = (int)_wcfProcessIdCounters[instance].NextValue();

                    if (!_wcfMemoryUsageCounters.ContainsKey(instance))
                        _wcfMemoryUsageCounters[instance] = new PerformanceCounter("Process", "Working Set - Private", instance, true);
                    entry.MemoryUsage = _wcfMemoryUsageCounters[instance].NextValue();

                    if (!_wcfThreadCountreCounters.ContainsKey(instance))
                        _wcfThreadCountreCounters[instance] = new PerformanceCounter("Process", "Thread Count", instance, true);
                    entry.ThreadCount = (int)_wcfThreadCountreCounters[instance].NextValue();

                    if (!_wcfProcessorUsageCounters.ContainsKey(instance))
                        _wcfProcessorUsageCounters[instance] = new WmiPerformanceCounter();//"Process", "% Processor Time", instance, true);
                    entry.ProcessorUsage = _wcfProcessorUsageCounters[instance].NextValue<float>("SELECT PercentProcessorTime FROM Win32_PerfFormattedData_PerfProc_Process WHERE IDProcess =" + entry.ProcessId, "PercentProcessorTime");

                    foreach (var item in new System.Management.ManagementObjectSearcher("Select NumberOfProcessors, NumberOfLogicalProcessors from Win32_ComputerSystem").Get())
                    {
                        entry.NumberOfProcessors += int.Parse(item["NumberOfProcessors"].ToString());
                        entry.NumberOfLogicalProcessors += int.Parse(item["NumberOfLogicalProcessors"].ToString());
                    }

                    foreach (var item in new System.Management.ManagementObjectSearcher("Select NumberOfCores from Win32_Processor").Get())
                    {
                        entry.NumberOfCores += int.Parse(item["NumberOfCores"].ToString());
                    }

                    EntryQueue.Register(entry);
                }
                catch (Exception exception)
                {
                    LogManager.WriteError("Unable to harvest wcf process resource usage: " + exception.Message);
                }
            });
        }
    }
}