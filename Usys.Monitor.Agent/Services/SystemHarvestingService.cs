﻿using System.ServiceProcess;
using Usys.Monitor.Agent.ServiceThreads;
using Usys.Monitor.Core.Diagnostics;

namespace Usys.Monitor.Agent.Services
{
    partial class SystemHarvestingService : ServiceBase
    {
        private readonly ServiceThreadBase _updateConfigurationSectionServiceThreadService = new UpdateConfigurationSectionServiceThread();
        private readonly ServiceThreadBase _registerEntriesThreadService = new RegisterEntriesServiceThread();
        private readonly ServiceThreadBase _updateServiceConfigThreadService = new UpdateServiceConfigServiceThread();

        private readonly ServiceThreadBase _generalPerformanceThreadService = new GeneralPerformanceServiceThread();
        private readonly ServiceThreadBase _mvcPerformanceThreadService = new MvcPerformanceServiceThread();
        private readonly ServiceThreadBase _wcfPerformanceThreadService = new WcfPerformanceServiceThread();
        private readonly ServiceThreadBase _telnetPerformanceThreadService = new TelnetPerformanceServiceThread();
        private readonly ServiceThreadBase _docGenPerformanceThreadService = new DocGenPerformanceServiceThread();

        public SystemHarvestingService()
        {
            InitializeComponent();
        }

        public void Start(params string[] args)
        {
            OnStart(args);
        }

        protected override void OnStart(string[] args)
        {
            LogManager.WriteInfo("Starting harvesting service.");

            _updateConfigurationSectionServiceThreadService.Start();
            _updateServiceConfigThreadService.Start();
            _registerEntriesThreadService.Start();

            _generalPerformanceThreadService.Start();
            _mvcPerformanceThreadService.Start();
            _wcfPerformanceThreadService.Start();
            _telnetPerformanceThreadService.Start();
            _docGenPerformanceThreadService.Start();
        }

        protected override void OnStop()
        {
            _updateConfigurationSectionServiceThreadService.Stop();
            _generalPerformanceThreadService.Stop();
            _registerEntriesThreadService.Stop();

            _generalPerformanceThreadService.Stop();
            _mvcPerformanceThreadService.Stop();
            _wcfPerformanceThreadService.Stop();
            _telnetPerformanceThreadService.Stop();
            _docGenPerformanceThreadService.Stop();
        }
    }
}
