﻿using System;
using Usys.Monitor.Agent;
using Usys.Monitor.Controller;

namespace Usys.Monitor.Console
{
    public class Program
    {
        public static void Main(string[] args)
        {
            new AgentServiceStarter().Start();
            new ControllerServiceStarter().Start();
            //System.Console.ReadKey();

            System.Console.WriteLine(Environment.ProcessorCount);
            System.Console.ReadKey();
        }
    }
}
