﻿using Usys.Monitor.Controller.Services;

namespace Usys.Monitor.Controller
{
    public class ControllerServiceStarter
    {
        private AgentCommunicationService _agentCommunicationService;

        public void Start()
        {
            _agentCommunicationService = new AgentCommunicationService();
            _agentCommunicationService.Start();
        }
    }
}
