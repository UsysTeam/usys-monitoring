﻿using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using Usys.Monitor.Data.Model;

namespace Usys.Monitor.Controller.Data
{
    public class Repository : DbContext
    {
        public Repository()
            : base("Repository")
        {
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            //Remove pluralizing table names
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            modelBuilder.Entity<ServerConfig>().HasKey(sc => sc.MachineName);
        }

        public IDbSet<GeneralDriveInfo> GeneralDriveInfos { get; set; }
        public IDbSet<GeneralSystemInfo> GeneralSystemInfos { get; set; }
        public IDbSet<ServerConfig> ServerConfigs { get; set; }
        public IDbSet<MvcProcessInfo> MvcProcessInfos { get; set; }
        public IDbSet<WcfProcessInfo> WcfProcessInfos { get; set; }
        public IDbSet<TelnetProcessInfo> TelnetProcessInfos { get; set; }
        public IDbSet<DocGenProcessInfo> DocGenProcessInfos { get; set; }
    }
}
