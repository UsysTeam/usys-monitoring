﻿using System;
using System.Collections.Generic;
using System.Linq;
using Usys.Monitor.Controller.Data;
using Usys.Monitor.Core.Diagnostics;
using Usys.Monitor.Data.Model;
using Usys.Monitor.Service.Contracts.ServiceContracts;

namespace Usys.Monitor.Controller.Implementations
{
    public class AgentControllerService : IAgentControllerService
    {
        public bool RegisterEntries(List<EntryBase> entries)
        {
            try
            {
                var repository = new Repository();

                foreach (var entry in entries)
                {
                    var set = repository.Set(entry.GetType());
                    set.Add(entry);
                }
                repository.SaveChanges();

                return true;
            }
            catch (Exception exception)
            {
                LogManager.WriteError("Unable to register entries: " + exception.Message);
                return false;
            }
        }

        public ServerConfig RetrieveServerConfig(string machineName)
        {
            try
            {
                var repository = new Repository();

                var serverConfig = repository.ServerConfigs.FirstOrDefault(config => config.MachineName == machineName);

                return serverConfig;
            }
            catch (Exception exception)
            {
                LogManager.WriteError("Unable to retrieve server config: " + exception.Message);
                return null;
            }
        }
    }
}
