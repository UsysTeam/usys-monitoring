﻿using System;
using System.Configuration.Install;
using System.Reflection;
using System.ServiceProcess;
using Usys.Monitor.Controller.Services;
using Usys.Monitor.Core.Diagnostics;

namespace Usys.Monitor.Controller
{
    public static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        public static void Main(params string[] args)
        {
            LogManager.WriteInfo("Starting Controller.");
            if (Environment.UserInteractive)
            {
                string parameter = string.Concat(args);
                switch (parameter)
                {
                    case "--install":
                        ManagedInstallerClass.InstallHelper(new[] {Assembly.GetExecutingAssembly().Location});
                        break;
                    case "--uninstall":
                        ManagedInstallerClass.InstallHelper(new[] {"/u", Assembly.GetExecutingAssembly().Location});
                        break;
                }
            }
            else
            {
                var servicesToRun = new ServiceBase[]
                {
                    new AgentCommunicationService(),
                };
                ServiceBase.Run(servicesToRun);
            }
        }
    }
}
