﻿using System.ComponentModel;
using System.Configuration.Install;
using System.ServiceProcess;

namespace Usys.Monitor.Controller
{
    [RunInstaller(true)]
    public class ProjectInstaller : Installer
    {
        private ServiceProcessInstaller process;
        private ServiceInstaller service;

        public ProjectInstaller()
        {
            process = new ServiceProcessInstaller();
            process.Account = ServiceAccount.User;
            process.Username = "VanEyndS@prdlog.local";
            process.Password = "Univeg01";

            service = new ServiceInstaller();
            service.ServiceName = "Usys Monitor Controller";
            service.StartType = ServiceStartMode.Automatic;

            Installers.Add(process);
            Installers.Add(service);
        }
    }
}
