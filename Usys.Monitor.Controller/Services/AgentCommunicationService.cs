﻿using System;
using System.ServiceModel;
using System.ServiceProcess;
using Usys.Monitor.Controller.Implementations;
using Usys.Monitor.Core.Diagnostics;

namespace Usys.Monitor.Controller.Services
{
    partial class AgentCommunicationService : ServiceBase
    {
        private ServiceHost _serviceHost;

        public AgentCommunicationService()
        {
            InitializeComponent();
        }

        public void Start(params string[] args)
        {
            OnStart(args);
        }

        protected override void OnStart(string[] args)
        {
            try
            {
                LogManager.WriteInfo("Starting communication service host.");
                if (_serviceHost != null)
                    _serviceHost.Close();

                _serviceHost = new ServiceHost(typeof(AgentControllerService));
                _serviceHost.Open();
            }
            catch (Exception exception)
            {
                LogManager.WriteError("Unable to start communication service host: " + exception.Message);
            }
        }

        protected override void OnStop()
        {
            _serviceHost.Close();
            _serviceHost = null;
        }
    }
}
