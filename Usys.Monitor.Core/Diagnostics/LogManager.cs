﻿using System.Diagnostics;

namespace Usys.Monitor.Core.Diagnostics
{
    public static class LogManager
    {
        private const string Source = "Usys Monitor";
        private static readonly object SourceLock = new object();
        private const string Log = "Application";

        public static void Write(string message, EventLogEntryType logEntryType)
        {
            AssureEventSource();

            EventLog.WriteEntry(Source, message, logEntryType);
        }

        public static void WriteInfo(string message)
        {
            Write(message, EventLogEntryType.Information);
        }

        public static void WriteWarning(string message)
        {
            Write(message, EventLogEntryType.Warning);
        }

        public static void WriteError(string message)
        {
            Write(message, EventLogEntryType.Error);
        }

        private static void AssureEventSource()
        {
            if (!EventLog.SourceExists(Source))
            {
                lock (SourceLock)
                {
                    if (!EventLog.SourceExists(Source))
                    {
                        EventLog.CreateEventSource(Source, Log);
                    }
                }
            }
        }
    }
}
