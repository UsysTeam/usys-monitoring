﻿using System;
using System.Runtime.Serialization;

namespace Usys.Monitor.Data.Model
{
    [DataContract]
    [KnownType(typeof(GeneralDriveInfo))]
    [KnownType(typeof(GeneralSystemInfo))]
    [KnownType(typeof(MvcProcessInfo))]
    [KnownType(typeof(WcfProcessInfo))]
    [KnownType(typeof(TelnetProcessInfo))]
    [KnownType(typeof(DocGenProcessInfo))]
    public abstract class EntryBase
    {
        [DataMember]
        public Guid Id { get; set; }

        [DataMember]
        public string MachineName { get; set; }

        [DataMember]
        public DateTime CreationDateTime { get; set; }
    }
}