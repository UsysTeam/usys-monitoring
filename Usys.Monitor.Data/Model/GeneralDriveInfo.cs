﻿using System.Runtime.Serialization;

namespace Usys.Monitor.Data.Model
{
    [DataContract]
    public class GeneralDriveInfo : EntryBase
    {
        [DataMember]
        public long AvailableFreeSpace { get; set; }

        [DataMember]
        public long TotalFreeSpace { get; set; }

        [DataMember]
        public long TotalSize { get; set; }
    }
}
