﻿using System.Runtime.Serialization;

namespace Usys.Monitor.Data.Model
{
    [DataContract]
    public class GeneralSystemInfo : EntryBase
    {
        [DataMember]
        public float ProcessorUsage { get; set; }

        [DataMember]
        public float MemoryUsage { get; set; }

        [DataMember]
        public int NumberOfProcessors { get; set; }

        [DataMember]
        public int NumberOfLogicalProcessors { get; set; }

        [DataMember]
        public int NumberOfCores { get; set; }
    }
}