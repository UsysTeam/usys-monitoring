﻿using System.Runtime.Serialization;

namespace Usys.Monitor.Data.Model
{
    [DataContract]
    public class MvcProcessInfo : EntryBase
    {
        [DataMember]
        public int ProcessId { get; set; }

        [DataMember]
        public int ThreadCount { get; set; }

        [DataMember]
        public float MemoryUsage { get; set; }

        [DataMember]
        public float ProcessorUsage { get; set; }

        [DataMember]
        public string InstanceName { get; set; }

        [DataMember]
        public int NumberOfProcessors { get; set; }

        [DataMember]
        public int NumberOfLogicalProcessors { get; set; }

        [DataMember]
        public int NumberOfCores { get; set; }
    }
}