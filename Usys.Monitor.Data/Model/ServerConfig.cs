﻿using System.Runtime.Serialization;

namespace Usys.Monitor.Data.Model
{
    [DataContract]
    public class ServerConfig
    {
        [DataMember]
        public string MachineName { get; set; }

        [DataMember]
        public bool IsTelnetServer { get; set; }

        [DataMember]
        public bool IsDocGenServer { get; set; }

        [DataMember]
        public bool IsMvcServer { get; set; }

        [DataMember]
        public bool IsWcfServer { get; set; }
    }
}
