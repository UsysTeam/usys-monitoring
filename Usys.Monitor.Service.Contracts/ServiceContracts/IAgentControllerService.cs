﻿using System.Collections.Generic;
using System.ServiceModel;
using Usys.Monitor.Data.Model;

namespace Usys.Monitor.Service.Contracts.ServiceContracts
{
    [ServiceContract]
    public interface IAgentControllerService
    {
        [OperationContract]
        bool RegisterEntries(List<EntryBase> entries);

        [OperationContract]
        ServerConfig RetrieveServerConfig(string machineName);
    }
}
