﻿using System.Diagnostics;
using System.Web.Optimization;

namespace Usys.Monitor.WebShell
{
    public class BundleConfig
    {
        public const string KendoVersion = "2014.2.801"; // Don't forget to manually change the version in .css files

        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            bundles.Add(new ScriptBundle("~/bundles/kendoScripts").Include(
                "~/Scripts/kendo/" + KendoVersion + "/kendo.all.min.js",
                "~/Scripts/kendo/" + KendoVersion + "/kendo.aspnetmvc.min.js"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css"));

            bundles.Add(new StyleBundle("~/Content/kendo").Include(
                "~/Content/kendo/" + KendoVersion + "/kendo.common.min.css",
                "~/Content/kendo/" + KendoVersion + "/kendo.bootstrap.min.css"));

            bundles.Add(new StyleBundle("~/Content/cssMain").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.main.css"));

            bundles.Add(new StyleBundle("~/Content/kendoMain").Include(
                "~/Content/kendo/" + KendoVersion + "/kendo.common.min.css",
                "~/Content/kendo/" + KendoVersion + "/kendo.bootstrap.min.css"));

            // Set EnableOptimizations to false for debugging. For more information,
            // visit http://go.microsoft.com/fwlink/?LinkId=301862
            BundleTable.EnableOptimizations = Debugger.IsAttached;
        }
    }
}
