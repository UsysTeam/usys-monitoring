﻿using System;
using System.Linq;
using System.Web.Mvc;
using Usys.Monitor.WebShell.Data;

namespace Usys.Monitor.WebShell.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            return View();
        }

        public ActionResult GetWcfServerProcessorHealth()
        {
            try
            {
                var repository = new Repository();

                var summaries = repository.MvcProcessInfoDaySummaries.ToList();

                return Json(new
                {
                    AverageProcessorUsage = Convert.ToInt32(summaries.Average(r => r.AverageProcessorUsage)),
                    MaximumProcessorUsage = Convert.ToInt32(summaries.Average(r => r.MaximumProcessorUsage)),
                    AverageMemoryUsage = Convert.ToInt32(summaries.Average(r => r.AverageMemoryUsage) / 1024 / 1024),
                    MaximumMemoryUsage = Convert.ToInt32(summaries.Average(r => r.MaximumMemoryUsage) / 1024 / 1024)
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(new { AverageProcessorUsage = 100 }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult GetMvcServerProcessorHealth()
        {
            try
            {
                var repository = new Repository();

                var summaries = repository.MvcProcessInfoDaySummaries.ToList();

                return Json(new
                {
                    AverageProcessorUsage = Convert.ToInt32(summaries.Average(r => r.AverageProcessorUsage)),
                    MaximumProcessorUsage = Convert.ToInt32(summaries.Average(r => r.MaximumProcessorUsage)),
                    AverageMemoryUsage = Convert.ToInt32(summaries.Average(r => r.AverageMemoryUsage) / 1024 / 1024),
                    MaximumMemoryUsage = Convert.ToInt32(summaries.Average(r => r.MaximumMemoryUsage) / 1024 / 1024),
                    ServerCount = summaries.GroupBy(m => m.MachineName).Count(),
                    InstanceCount = summaries.Count()
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(new
                {
                    AverageProcessorUsage = 0,
                    MaximumProcessorUsage = 0,
                    AverageMemoryUsage = 0,
                    MaximumMemoryUsage = 0
                }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}