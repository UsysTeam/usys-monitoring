﻿using System;
using System.Linq;
using System.Web.Mvc;
using Usys.Monitor.WebShell.Data;
using WebGrease;

namespace Usys.Monitor.WebShell.Controllers
{
    public class MainController : Controller
    {
        public ActionResult Index()
        {
            var repository = new Repository();

            var servers = repository.ServerConfigs.ToList();

            return View(servers);
        }

        public ActionResult ServerLastMinutes(string id)
        {
            return PartialView("_ServerLastHour", id);
        }

        public ActionResult RefreshMvcData()
        {
            var repository = new Repository();

            var averages = repository.GeneralSystemInfoCurrents.Where(s => s.IsMvcServer).ToList();

            return Json(averages, JsonRequestBehavior.AllowGet);
        }

        public ActionResult RefreshWcfData()
        {
            var repository = new Repository();

            var averages = repository.GeneralSystemInfoCurrents.Where(s => s.IsWcfServer).ToList();

            return Json(averages, JsonRequestBehavior.AllowGet);
        }

        public ActionResult RefreshLastHour(string id)
        {
            var repository = new Repository();

            var records = repository.GeneralSystemInfoLastMinutes.Where(s => s.MachineName == id).ToList().OrderBy(s => s.ElapsedSeconds);

            return Json(records, JsonRequestBehavior.AllowGet);
        }
    }
}