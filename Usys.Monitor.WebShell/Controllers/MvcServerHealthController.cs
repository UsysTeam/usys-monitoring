﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Usys.Monitor.WebShell.Data;
using Usys.Monitor.WebShell.Data.Model;

namespace Usys.Monitor.WebShell.Controllers
{
    public class MvcServerHealthController : Controller
    {
        public ActionResult Index()
        {
            var repository = new Repository();

            var servers = repository.MvcServerConfigs.OrderBy(m => m.MachineName).ToList();

            return View(servers);
        }

        public ActionResult ServerInfo(string id)
        {
            var repository = new Repository();

            var servers = new List<string>();

            if (!string.IsNullOrWhiteSpace(id))
            {
                //return the detail view
                return PartialView("_ServerInfoDetail", id);
            }

            servers.AddRange(repository.MvcServerConfigs.Select(m => m.MachineName));
            return PartialView("_ServerInfo", servers);

        }

        public ActionResult AverageProcessorUsage(string id)
        {
            var repository = new Repository();

            var lower = DateTime.UtcNow.AddDays(-1);

            var hours = repository.MvcProcessInfoHours
                .Where(e => e.MachineName == id)
                .Where(e => e.CreationDateTime > lower)
                .Select(e => new { e.AverageProcessorUsage, e.MaximumProcessorUsage, e.CreationDateTime })
                .ToList();

            var result = Json(hours, JsonRequestBehavior.AllowGet);
            result.MaxJsonLength = int.MaxValue;
            return result;
        }

        public ActionResult AverageMemoryUsage(string id)
        {
            var repository = new Repository();

            var lower = DateTime.UtcNow.AddDays(-1);

            var hours = repository.MvcProcessInfoHours
                .Where(e => e.MachineName == id)
                .Where(e => e.CreationDateTime > lower)
                .Select(e => new
                {
                    AverageMemoryUsage = e.AverageMemoryUsage / 1024 / 1024,
                    MaximumMemoryUsage = e.MaximumMemoryUsage / 1024 / 1024,
                    e.CreationDateTime
                })
                .ToList();

            var result = Json(hours, JsonRequestBehavior.AllowGet);
            result.MaxJsonLength = int.MaxValue;
            return result;
        }
    }
}