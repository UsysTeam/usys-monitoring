﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Usys.Monitor.WebShell.Data.Model
{
    [Table("GeneralSystemInfoCurrentView")]
    public class GeneralSystemInfoCurrent
    {
        public Guid Id { get; set; }
        public string MachineName { get; set; }
        public bool IsTelnetServer { get; set; }
        public bool IsDocGenServer { get; set; }
        public bool IsMvcServer { get; set; }
        public bool IsWcfServer { get; set; }
        public int AverageProcessorUsage { get; set; }
        public int MaximumProcessorUsage { get; set; }
    }
}