﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Usys.Monitor.WebShell.Data.Model
{
    [Table("GeneralSystemInfoLastMinuteView")]
    public class GeneralSystemInfoLastMinute
    {
        public Guid Id { get; set; }
        public string MachineName { get; set; }
        public int ElapsedSeconds { get; set; }
        public int AverageProcessorUsage { get; set; }
        public int MaximumProcessorUsage { get; set; }
    }
}