﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Usys.Monitor.WebShell.Data.Model
{
    [Table("MvcProcessInfoDayView")]
    public class MvcProcessInfoDay
    {
        public string MachineName { get; set; }
        public string InstanceName { get; set; }
        public DateTime CreationDateTime{ get; set; }
        public int AverageThreadCount { get; set; }
        public float AverageMemoryUsage { get; set; }
        public float AverageProcessorUsage { get; set; }
    }
}