﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Usys.Monitor.WebShell.Data.Model
{
    [Table("MvcProcessInfoDaySummaryView")]
    public class MvcProcessInfoDaySummary
    {
        public string MachineName { get; set; }
        public string InstanceName { get; set; }
        public decimal AverageProcessorUsage { get; set; }
        public decimal MaximumProcessorUsage { get; set; }
        public long AverageMemoryUsage { get; set; }
        public long MaximumMemoryUsage { get; set; }
    }
}