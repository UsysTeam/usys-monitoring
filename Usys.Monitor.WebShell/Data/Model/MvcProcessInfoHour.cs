﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Usys.Monitor.WebShell.Data.Model
{
    [Table("MvcProcessInfoHourView")]
    public class MvcProcessInfoHour
    {
        public Guid Id { get; set; }
        public string MachineName { get; set; }
        public DateTime CreationDateTime{ get; set; }
        public decimal AverageThreadCount { get; set; }
        public decimal AverageMemoryUsage { get; set; }
        public decimal AverageProcessorUsage { get; set; }
        public decimal MaximumThreadCount { get; set; }
        public decimal MaximumMemoryUsage { get; set; }
        public decimal MaximumProcessorUsage { get; set; }
    }
}