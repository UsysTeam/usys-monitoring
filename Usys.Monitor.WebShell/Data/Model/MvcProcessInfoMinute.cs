﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Usys.Monitor.WebShell.Data.Model
{
    [Table("MvcProcessInfoMinuteView")]
    public class MvcProcessInfoMinute
    {
        public string MachineName { get; set; }
        public string InstanceName { get; set; }
        public DateTime CreationDateTime{ get; set; }
        public decimal AverageThreadCount { get; set; }
        public decimal AverageMemoryUsage { get; set; }
        public decimal AverageProcessorUsage { get; set; }
        public decimal MaximumThreadCount { get; set; }
        public decimal MaximumMemoryUsage { get; set; }
        public decimal MaximumProcessorUsage { get; set; }
    }
}