using System.ComponentModel.DataAnnotations.Schema;

namespace Usys.Monitor.WebShell.Data.Model
{
    [Table("MvcServerConfigView")]
    public class MvcServerConfig
    {
        public string MachineName { get; set; }
        public bool Active { get; set; }
    }
}