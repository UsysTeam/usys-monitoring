using System.ComponentModel.DataAnnotations.Schema;

namespace Usys.Monitor.WebShell.Data.Model
{
    [Table("ServerConfigView")]
    public class ServerConfig
    {
        public string MachineName { get; set; }
        public bool IsTelnetServer { get; set; }
        public bool IsDocGenServer { get; set; }
        public bool IsMvcServer { get; set; }
        public bool IsWcfServer { get; set; }
    }
}