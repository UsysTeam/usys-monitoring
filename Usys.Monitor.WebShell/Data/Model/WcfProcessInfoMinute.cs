﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Usys.Monitor.WebShell.Data.Model
{
    [Table("WcfProcessInfoMinuteView")]
    public class WcfProcessInfoMinute
    {
        public string MachineName { get; set; }
        public string InstanceName { get; set; }
        public DateTime CreationDateTime{ get; set; }
        public int AverageThreadCount { get; set; }
        public long AverageMemoryUsage { get; set; }
        public decimal AverageProcessorUsage { get; set; }
    }
}