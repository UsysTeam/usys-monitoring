﻿using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using Usys.Monitor.WebShell.Data.Model;

namespace Usys.Monitor.WebShell.Data
{
    public class Repository : DbContext
    {
        public Repository()
            : base("DefaultConnection")
        {

        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            //Remove pluralizing table names
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            modelBuilder.Entity<WcfProcessInfoMinute>().HasKey(m => m.MachineName);

            modelBuilder.Entity<MvcProcessInfoMinute>().HasKey(m => m.MachineName);

            modelBuilder.Entity<MvcServerConfig>().HasKey(m => m.MachineName);
            modelBuilder.Entity<ServerConfig>().HasKey(m => m.MachineName);

            modelBuilder.Entity<MvcProcessInfoDaySummary>().HasKey(m => m.MachineName);

            modelBuilder.Entity<GeneralSystemInfoLastMinute>().Ignore(m => m.CreationDateTime);
        }

        public IDbSet<WcfProcessInfoMinute> WcfProcessInfoMinutes { get; set; }

        public IDbSet<MvcProcessInfoMinute> MvcProcessInfoMinutes { get; set; }
        public IDbSet<MvcProcessInfoHour> MvcProcessInfoHours { get; set; }

        public IDbSet<MvcServerConfig> MvcServerConfigs { get; set; }
        public IDbSet<ServerConfig> ServerConfigs { get; set; }

        public IDbSet<MvcProcessInfoDaySummary> MvcProcessInfoDaySummaries { get; set; }

        public IDbSet<GeneralSystemInfoCurrent> GeneralSystemInfoCurrents { get; set; }
        public IDbSet<GeneralSystemInfoLastMinute> GeneralSystemInfoLastMinutes { get; set; }
    }
}